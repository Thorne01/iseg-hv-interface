#ifndef ISEG_SHQ226L_ISEG_SHQ226L_H
#define ISEG_SHQ226L_ISEG_SHQ226L_H

#define ISEG_MSG_SIZE   512
#define ISEG_OK         0x0000 //means zero in decimal, equivalent to FUGHCB_SUCCESS
#define ISEG_COM_ERROR  0x0001 //means one in decimal, equivalent to FUGHCB_FAILURE

#define ISEG_ERROR_SYNTAX   0x1000
#define ISEG_ERROR_WCN      0x1001
#define ISEG_ERROR_TOT      0x1002
#define ISEG_ERROR_UMAX     0x1003
#define ISEG_ERROR_UKN      0x1004

#define ISEG_CONNECT_FAIL (-1)

int ISEG_T2DP66_connect(const char* port_id, char* error);
int ISEG_T2DP66_disconnect(const int serial_port, char* error);

int ISEG_T2DP66_write(const int serial_port, char* message, char* error);
int ISEG_T2DP66_read(const int serial_port, char* reply, char* error);

int ISEG_T2DP66_identify(const int serial_port, int channel, char* reply, char* error);

int ISEG_T2DP66_get_status(const int serial_port, const int channel, char* reply, char* error);
int ISEG_T2DP66_get_status_message(char* status_code, char* reply, char* error);

int ISEG_T2DP66_get_error_message(char* error_code, char* error_message);

int ISEG_T2DP66_get_voltage(const int serial_port, const int channel, char* reply, double* voltage, char* error);
int ISEG_T2DP66_get_current(const int serial_port, const int channel, char* reply, double* current, char* error);

int ISEG_T2DP66_set_current(const int serial_port, const int channel, double current, char* error);
int ISEG_T2DP66_set_voltage(const int serial_port, const int channel, double target_voltage, char* error);

int ISEG_T2DP66_measure_voltage(const int serial_port, const int channel, double* voltage, char* error);
int ISEG_T2DP66_measure_current(const int serial_port, const int channel, double* current, char* error);

int ISEG_T2DP66_set_polarity(const int serial_port, const int channel, bool polarity, char* error);
int ISEG_T2DP66_get_polarity(const int serial_port, const int channel, char* reply, char* error);

int ISEG_T2DP66_set_autostart(const int serial_port, const int channel, bool autostart, char* error);
int ISEG_T2DP66_get_autostart(const int serial_port, const int channel, char* reply, char* error);

int ISEG_T2DP66_set_trip(const int serial_port, const int channel, bool trip, char* error);
int ISEG_T2DP66_get_trip(const int serial_port, const int channel, char* reply, char* error);

int ISEG_T2DP66_set_single_echo(const int serial_port, const int channel, char* error);

#endif
