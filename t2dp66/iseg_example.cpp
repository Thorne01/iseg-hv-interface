#include "iseg_t2dp66.h"
#include "stdio.h"
#include <cmath>
#include <unistd.h>

int main()
{
    printf("\n +----------------------------------+");
    printf("\n |    ISEG T2DP66 HV PowerSupply    |");
    printf("\n +----------------------------------+\n\n");

    double target_voltage = 100;
    char error[ISEG_MSG_SIZE];

    /*------ Opening the Serial Port -----*/
    int fd = ISEG_T2DP66_connect("/dev/iseg_t2dp66", error);
    if(fd == -1){printf("%s\n",error); return -1;}
    char reply[ISEG_MSG_SIZE];
    double measured_voltage = 0.;
    double measured_current = 0.;
    double set_voltage = 0.;
    double set_current = 0.;
    char status_reply[ISEG_MSG_SIZE];
	int channel = 1;

	//if(ISEG_T2DP66_set_single_echo(fd, 1, error)){printf("%s\n", error); return -1;}

    if(ISEG_T2DP66_identify(fd, channel, reply, error)){printf("%s\n", error); return -1;}
    else{printf("ID: %s\n", reply);}

    if(ISEG_T2DP66_set_autostart(fd, channel, true, error)){printf("%s\n", error); return -1;}
    if(ISEG_T2DP66_get_autostart(fd, 1, reply, error)){printf("%s\n", error); return -1;}

    printf("%s\n", reply);

    if(ISEG_T2DP66_set_trip(fd, channel, true, error)){printf("%s\n", error); return -1;}
    if(ISEG_T2DP66_get_trip(fd, 1, reply, error)){printf("%s\n", error); return -1;}

    printf("%s\n", reply);

    if(ISEG_T2DP66_set_polarity(fd, channel, true, error)){printf("%s\n", error); return -1;}
    sleep(1);//this is required as less then 1 second the commands seem to build up before the device buffer can process them
    if(ISEG_T2DP66_get_polarity(fd, 1, reply, error)){printf("%s\n", error); return -1;}

    printf("%s\n", reply);

    if(ISEG_T2DP66_set_voltage(fd, channel, target_voltage, error)){printf("%s\n", error); return -1;}
    if(ISEG_T2DP66_set_current(fd, channel, 1, error)){printf("%s\n", error); return -1;}
    
    if(ISEG_T2DP66_get_voltage(fd, channel, reply, &set_voltage, error)){printf("%s\n", error); return -1;}
    if(ISEG_T2DP66_get_current(fd, channel, reply, &set_current, error)){printf("%s\n", error); return -1;}

    printf("Target Voltage: %lf\n", target_voltage);
    printf("Set Voltage: %lf\n", set_voltage);
    printf("Set Current: %lf\n", set_current);
    
    if(ISEG_T2DP66_get_status(fd, 1, status_reply, error)){printf("%s\n", error); return -1;}

    printf("Status: %s\n", status_reply);

    if(ISEG_T2DP66_measure_voltage(fd, 1, &measured_voltage, error)){printf("%s\n", error); return -1;}

    while (fabs(fabs(measured_voltage) - target_voltage) > 1.5)
    {
        if(ISEG_T2DP66_measure_voltage(fd, 1, &measured_voltage, error)){printf("%s\n", error); return -1;}
        if(ISEG_T2DP66_measure_current(fd, 1, &measured_current, error)){printf("%s\n", error); return -1;}
        printf("Measured V: %lf, Measured A: %lf \r", measured_voltage, measured_current);
        fflush(stdout);
    }
    sleep(5);
    printf("\n");
    target_voltage = 0.;
    if(ISEG_T2DP66_set_voltage(fd, 1, target_voltage, error)){printf("%s\n", error); return -1;}
    if(ISEG_T2DP66_get_voltage(fd, 1, reply, &set_voltage, error)){printf("%s\n", error); return -1;}
    printf("Set V: %lf\n",set_voltage);

    while(fabs(fabs(measured_voltage)-target_voltage) > 1.0)
    {
        if(ISEG_T2DP66_measure_voltage(fd, 1, &measured_voltage, error)){printf("%s\n", error); return -1;}
        if(ISEG_T2DP66_measure_current(fd, 1, &measured_current, error)){printf("%s\n", error); return -1;}
        printf("Measured V: %lf, Measured A: %lf \r", measured_voltage, measured_current);
        fflush(stdout);
    }
    printf("\n");
    if(ISEG_T2DP66_disconnect(fd, error)){printf("%s\n", error); return -1;}
    sleep(1);
    return ISEG_OK;
}
