#include "iseg_t2dp66.h"

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <cmath>
#include <termios.h>

struct termios tty_opt;

/// connect to ISEG HV powersupply connected on serial com port
/// \param port_id string of path to serial file
/// \return serial_port file descriptor
int ISEG_T2DP66_connect(const char* port_id, char* error)
{
    memset(error, '\0', ISEG_MSG_SIZE* sizeof(char));
    int serial_port = open(port_id, (O_RDWR|O_SYNC|O_NOCTTY) );
    if(serial_port<0){sprintf(error, "errno: %s", strerror(errno)); return ISEG_CONNECT_FAIL;}
    struct termios tty_opt_new;
    tcgetattr(serial_port, &tty_opt); /* save current serial port settings */
    bzero(&tty_opt_new, sizeof(tty_opt_new)); /* clear struct for new port settings */


    /*tty_opt_new.c_cflag &= ~PARENB;
    tty_opt_new.c_cflag &= ~CSTOPB;
    tty_opt_new.c_cflag &= ~CSIZE;
    tty_opt_new.c_cflag |= CS8|CLOCAL|CREAD;
    tty_opt_new.c_lflag |= ICANON;
    tty_opt_new.c_lflag &= ~(ECHO|ECHOE);
    tty_opt_new.c_iflag |= (IXON|IXOFF|IXANY);
    tty_opt_new.c_oflag |= OPOST; //raw: tty_opt_new.c_oflag &= ~OPOST;*/

    tty_opt_new.c_iflag |= IGNBRK; //input options
    tty_opt_new.c_cflag |= CS8 | CLOCAL | CREAD; //control options
    tty_opt_new.c_iflag &= ~(INLCR | IGNCR | ICRNL); //repeated
    tty_opt_new.c_oflag &= ~(ONLCR | OCRNL | OPOST); //output options
	//tty_opt_new.c_oflag |=0;
    tty_opt_new.c_lflag |= ICANON; // Canonical mode //line options
    tty_opt_new.c_lflag &= ~(ECHO | ECHOE | ECHONL | IEXTEN);

    tty_opt_new.c_cc[VMIN]  = 1;
    tty_opt_new.c_cc[VTIME] = 0;

	/*tty_opt_new.c_iflag &= ~(IGNBRK | BRKINT | ICRNL | INLCR | PARMRK | INPCK | ISTRIP | IXON);
	tty_opt_new.c_oflag = 0;
	tty_opt_new.c_lflag &= ~(ECHO | ECHONL | IEXTEN );
	tty_opt_new.c_lflag |= ICANON | ISIG;
	tty_opt_new.c_cflag &= ~(CSIZE | PARENB);
	tty_opt_new.c_cflag |= CS8;
   	tty_opt_new.c_cflag |= CLOCAL | CREAD;

	tty_opt_new.c_cc[VMIN]  = 10; 
 	tty_opt_new.c_cc[VTIME] = 0;
	tty_opt_new.c_cc[VEOL] = 0;
	tty_opt_new.c_cc[VEOL2] = 0;
	tty_opt_new.c_cc[VEOF] = 0x04;*/

    cfsetispeed(&tty_opt_new, B9600);
    cfsetospeed(&tty_opt_new, B9600);
    tcflush(serial_port, TCIOFLUSH);

    if(tcsetattr(serial_port, TCSANOW, &tty_opt_new) != 0)
    {
        sprintf(error, "ISEG error opening device, errno: %s", strerror(errno));
        return ISEG_CONNECT_FAIL;
    }
    return serial_port;
}

/// disconnect from ISEG HV power supply, freeing serial port file descriptor, reset old serial attr
/// \param serial_port file descriptor returned from ISEG_T2DP66_connect
/// \return 0 on success or errno
int ISEG_T2DP66_disconnect(const int serial_port, char* error)
{
    memset(error, '\0', ISEG_MSG_SIZE* sizeof(char));
    if(tcsetattr(serial_port, TCSANOW, &tty_opt) != 0)
    {
        sprintf(error, "ISEG error closing device, errno: %s", strerror(errno));
        return ISEG_CONNECT_FAIL;
    }
    return close(serial_port);
}

/// For internal use, write function for ISEG HV
/// \param serial_port file descriptor returned from ISEG_T2DP66_connect
/// \param message command to send to ISEG HV power supply
/// \return ISEG_COM_ERROR on error or ISEG_OK on all clear
int ISEG_T2DP66_write(const int serial_port, char* message, char* error)
{
    memset(error, '\0', ISEG_MSG_SIZE* sizeof(char));
    char iseg_msg[ISEG_MSG_SIZE];
    memset(iseg_msg,'\0',sizeof(char)*ISEG_MSG_SIZE);
    sprintf(iseg_msg, "%s\r\n",message);
    int bytes_to_write = sizeof(char)*strlen(iseg_msg);
    int bytes_written = write(serial_port, iseg_msg, bytes_to_write);
    if(bytes_to_write != bytes_written){sprintf(error, "ISEG bytes to write does not match bytes written"); return ISEG_COM_ERROR;}
    return ISEG_OK;
}

/// For internal use, read function for ISEG HV
/// \param serial_port file descriptor returned from ISEG_T2DP66_connect
/// \param reply holds response or error message
/// \return ISEG_COM_ERROR on error or ISEG_OK on all clear
int ISEG_T2DP66_read(const int serial_port, char* reply, char* error)
{
    memset(reply, '\0', ISEG_MSG_SIZE* sizeof(char));
    memset(error, '\0', ISEG_MSG_SIZE* sizeof(char));
    char iseg_reply[ISEG_MSG_SIZE];
    memset(iseg_reply,'\0',sizeof(char)*ISEG_MSG_SIZE);
    int bytes_read = read(serial_port, iseg_reply, sizeof(char)*ISEG_MSG_SIZE);
    // check for error on the reply
    iseg_reply[strcspn(iseg_reply, "\r\n")] = '\0'; // remove carriage return, new line
	
    if((strncmp(iseg_reply,"?",1) == 0) || bytes_read == 0)
    {
        char error_message[ISEG_MSG_SIZE];
        if(ISEG_T2DP66_get_error_message(iseg_reply,error_message))
        {
            sprintf(error, "ISEG failure to get error message in read");
            return ISEG_COM_ERROR;
        }
        sprintf(error, "ISEG error in read: %s", error_message);
        return ISEG_COM_ERROR;
    }
    else
    {
        memset(reply,'\0',sizeof(char)*ISEG_MSG_SIZE);
        strcpy(reply,iseg_reply);
    }
	usleep(20000);//After receiving the answer, the device needs some time to do internal cleanup.
				//Give it approximately 20 ms before sending the next command.
    return ISEG_OK;
}


/// Gets the error message as written in the ISEG manual
/// \param error_code string error code returned from device
/// \param error_message string error message as in manual
/// \return ISEG_COM_ERROR on error or ISEG_OK on all clear
int ISEG_T2DP66_get_error_message(char* error_code, char* reply)
{
    memset(reply, '\0', ISEG_MSG_SIZE* sizeof(char));
    unsigned int code = 0x0U;
    char error_message[ISEG_MSG_SIZE];
    code = strcmp(error_code,"????") == 0x0U ?          ISEG_ERROR_SYNTAX   : code;
    code = strcmp(error_code,"?WCN") == 0x0U ?         ISEG_ERROR_WCN      : code;
    code = strcmp(error_code,"?TOT") == 0x0U ?          ISEG_ERROR_TOT      : code;
    code = strncmp(error_code,"? UMAX=",6) == 0x0U ?    ISEG_ERROR_UMAX     : code;
    code = strcmp(error_code,"") == 0x0U ?              ISEG_ERROR_UKN      : code;
    memset(error_message,'\0',ISEG_MSG_SIZE*sizeof(char));
    double volt_limit=0.;

    switch(code)
    {
        case ISEG_ERROR_SYNTAX:
            sprintf(reply,"Syntax error");
            break;
        case ISEG_ERROR_WCN:
            sprintf(reply,"Wrong channel number");
            break;
        case ISEG_ERROR_TOT:
            sprintf(reply,"Timeout error");
            break;
        case ISEG_ERROR_UMAX:
            sscanf(error_code,"%*s%lf\r\n",&volt_limit);
            sprintf(reply,"Set voltage exceeds voltage limit %f",volt_limit);
            break;
        default:
            sprintf(reply,"Unknown error code: %s", error_code);
            break;
    }
    return ISEG_OK;
}

/// Gets the status message as written in the ISEG manual
/// The device returns two hex digits which is defined by high and low binary values in the programming manual
int ISEG_T2DP66_get_status_message(char* status_code, char* reply, char* error)
{
    memset(reply, '\0', ISEG_MSG_SIZE* sizeof(char));
    char channel_str[ISEG_MSG_SIZE];
    char long_reply[ISEG_MSG_SIZE];
    memset(channel_str, '\0', ISEG_MSG_SIZE* sizeof(char));
    memset(long_reply, '\0', ISEG_MSG_SIZE* sizeof(char));
    switch (status_code[0]) {
        case '0':
            sprintf(channel_str,"HV-OFF, ");
            break;
        case '1':
            sprintf(channel_str,"HV-OFF, Negative, ");
            break;
        case '2':
            sprintf(channel_str,"HV-ON, ");
            break;
        case '3':
            sprintf(channel_str,"HV-ON, Negative, ");
            break;
        case '4':
            sprintf(channel_str,"Kill enabled, HV-OFF, ");
            break;
        case '5':
            sprintf(channel_str,"Kill enabled, Negative, ");
            break;
        case '6':
            sprintf(channel_str,"Kill enabled, HV-ON, ");
            break;
        case '7':
            sprintf(channel_str,"Kill enabled, HV-ON, Negative, ");
            break;
        case '8':
            sprintf(channel_str,"Trip, limit reached -> HV-OFF, ");
            break;
        case '9':
            sprintf(channel_str,"Trip, limit reached -> HV-OFF, Negative, ");
            break;
        case 'C':
            sprintf(channel_str,"Trip, limit reached -> HV-OFF, Kill enabled, ");
            break;
        case 'D':
            sprintf(channel_str,"Trip, limit reached -> HV-OFF, Kill enabled, Negative, ");
            break;
        default:
            sprintf(error, "Unknown error on high bit"); return ISEG_COM_ERROR;
    }

    strcat(long_reply, channel_str);
    memset(channel_str, '\0', ISEG_MSG_SIZE* sizeof(char));
    switch (status_code[1]) {
        case '0':
            sprintf(channel_str,"Control mode reserved");
            break;
        case '1':
            sprintf(channel_str,"USB");
            break;
        case '2':
            sprintf(channel_str,"Local control");
            break;
        case '3':
            sprintf(channel_str,"Analog I/O");
            break;
        case '4':
            sprintf(channel_str,"Autostart enabled, Control mode reserved");
            break;
        case '5':
            sprintf(channel_str,"Autostart enabled, USB");
            break;
        case '6':
            sprintf(channel_str,"Autostart enabled, Local control");
            break;
        case '7':
            sprintf(channel_str,"Autostart enabled, Analog I/O");
            break;
        case '8':
            sprintf(channel_str,"Positive, Control mode reserved");
            break;
        case '9':
            sprintf(channel_str,"Positive, USB");
            break;
        case 'A':
            sprintf(channel_str,"Positive, Local control");
            break;
        case 'B':
            sprintf(channel_str,"Positive, Analog I/O");
            break;
        case 'C':
            sprintf(channel_str,"Positive, Autostart enabled, Control mode reserved");
            break;
        case 'D':
            sprintf(channel_str,"Positive, Autostart enabled, USB");
            break;
        case 'E':
            sprintf(channel_str,"Positive, Autostart enabled, Local control");
            break;
        case 'F':
            sprintf(channel_str,"Positive, Autostart enabled, Analog I/O");
            break;
        default:
            sprintf(error, "Unknown error on low bit"); return ISEG_COM_ERROR;//todo: possible memory issue with this function but unable to replicate it
    }
    strcat(long_reply, channel_str);
    sprintf(reply, "%s", long_reply);
    return ISEG_OK;
}

/// Gets ISEG HV power supply device infomation: Serial number; software release; V out max ; I out max
/// \param serial_port file descriptor returned from ISEG_T2DP66_connect
/// \param reply holds response or message
/// \return ISEG_COM_ERROR on error or ISEG_OK on all clear
int ISEG_T2DP66_identify(const int serial_port, const int channel, char* reply, char* error)
{
    char msg[ISEG_MSG_SIZE];
    char iseg_reply[ISEG_MSG_SIZE];
    char err[ISEG_MSG_SIZE];
	char null[ISEG_MSG_SIZE];

    memset(reply,'\0',sizeof(char)*ISEG_MSG_SIZE);
    memset(error,'\0',sizeof(char)*ISEG_MSG_SIZE);
	memset(null,'\0',sizeof(char)*ISEG_MSG_SIZE);

    sprintf(msg, "#%d", channel);

    if(ISEG_T2DP66_write(serial_port, msg, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    // ISEG replies with the command you sent, new line, then the response. So, we need to read twice
    if(ISEG_T2DP66_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
	if(iseg_reply[0] != '#'){
    	strcpy(reply, null);
		sprintf(error, "Returned message unreadable");
		return ISEG_COM_ERROR;
	}

    memset(iseg_reply,'\0',sizeof(char)*ISEG_MSG_SIZE);
    if(ISEG_T2DP66_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
	strcpy(reply, iseg_reply);
    return ISEG_OK;
}

int ISEG_T2DP66_set_polarity(const int serial_port, const int channel, bool polarity, char* error)
{
    char msg[ISEG_MSG_SIZE];
    char iseg_reply[ISEG_MSG_SIZE];
    char err[ISEG_MSG_SIZE];

    memset(error,'\0',sizeof(char)*ISEG_MSG_SIZE);
    if(polarity){sprintf(msg, "P%d=+", channel);}
    else(sprintf(msg, "P%d=-", channel));
    if(ISEG_T2DP66_write(serial_port, msg, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    // ISEG replies with the command you sent, new line, then the response. So, we need to read twice
    if(ISEG_T2DP66_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    //memset(iseg_reply,'\0',sizeof(char)*ISEG_MSG_SIZE);
    //if(ISEG_T2DP66_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    return ISEG_OK;
}

int ISEG_T2DP66_get_polarity(const int serial_port, const int channel, char* reply, char* error)
{
    char msg[ISEG_MSG_SIZE];
    char iseg_reply[ISEG_MSG_SIZE];
    char err[ISEG_MSG_SIZE];
	char null[ISEG_MSG_SIZE];

	memset(null,'\0',sizeof(char)*ISEG_MSG_SIZE);
    memset(reply,'\0',sizeof(char)*ISEG_MSG_SIZE);
    memset(error,'\0',sizeof(char)*ISEG_MSG_SIZE);

    sprintf(msg, "P%d", channel);
    if(ISEG_T2DP66_write(serial_port, msg, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    // ISEG replies with the command you sent, new line, then the response. So, we need to read twice
    if(ISEG_T2DP66_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    memset(iseg_reply,'\0',sizeof(char)*ISEG_MSG_SIZE);
    if(ISEG_T2DP66_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    //sprintf(reply, "Polarity set: %s", iseg_reply);
	if(iseg_reply[0] == '+'|| '-'){
    	sprintf(reply, "Polarity set: %s", iseg_reply);
	}
	else{strcpy(reply, null);sprintf(error, "Returned message unreadable");return ISEG_COM_ERROR;}

    return ISEG_OK;
}

int ISEG_T2DP66_set_autostart(const int serial_port, const int channel, bool autostart, char* error)
{
    char msg[ISEG_MSG_SIZE];
    char iseg_reply[ISEG_MSG_SIZE];
    char err[ISEG_MSG_SIZE];

    memset(error,'\0',sizeof(char)*ISEG_MSG_SIZE);

    sprintf(msg, "A%d=%d", channel, autostart);

    if(ISEG_T2DP66_write(serial_port, msg, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    // ISEG replies with the command you sent, new line, then the response. So, we need to read twice
    if(ISEG_T2DP66_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    //memset(iseg_reply,'\0',sizeof(char)*ISEG_MSG_SIZE);
    //if(ISEG_T2DP66_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    return ISEG_OK;
}

int ISEG_T2DP66_get_autostart(const int serial_port, const int channel, char* reply, char* error)
{
    char msg[ISEG_MSG_SIZE];
    char iseg_reply[ISEG_MSG_SIZE];
    char err[ISEG_MSG_SIZE];
	char null[ISEG_MSG_SIZE];

	memset(null,'\0',sizeof(char)*ISEG_MSG_SIZE);
    memset(reply,'\0',sizeof(char)*ISEG_MSG_SIZE);
    memset(error,'\0',sizeof(char)*ISEG_MSG_SIZE);

    sprintf(msg, "A%d", channel);
    if(ISEG_T2DP66_write(serial_port, msg, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    // ISEG replies with the command you sent, new line, then the response. So, we need to read twice
    if(ISEG_T2DP66_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    memset(iseg_reply,'\0',sizeof(char)*ISEG_MSG_SIZE);
    if(ISEG_T2DP66_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}

	if(iseg_reply[0] == '0'|| '1'){
    	sprintf(reply, "Autostart setting: %s", iseg_reply);
	}
	else{strcpy(reply, null);sprintf(error, "Returned message unreadable");return ISEG_COM_ERROR;}

    return ISEG_OK;
}

int ISEG_T2DP66_set_trip(const int serial_port, const int channel, bool trip, char* error)
{
    char msg[ISEG_MSG_SIZE];
    char iseg_reply[ISEG_MSG_SIZE];
    char err[ISEG_MSG_SIZE];

    memset(error,'\0',sizeof(char)*ISEG_MSG_SIZE);

    sprintf(msg, "T%d=%d", channel, trip);

    if(ISEG_T2DP66_write(serial_port, msg, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    // ISEG replies with the command you sent, new line, then the response. So, we need to read twice
    if(ISEG_T2DP66_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    //memset(iseg_reply,'\0',sizeof(char)*ISEG_MSG_SIZE);
    //if(ISEG_T2DP66_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    return ISEG_OK;
}

int ISEG_T2DP66_get_trip(const int serial_port, const int channel, char* reply, char* error)
{
    char msg[ISEG_MSG_SIZE];
    char iseg_reply[ISEG_MSG_SIZE];
    char err[ISEG_MSG_SIZE];
	char null[ISEG_MSG_SIZE];

	memset(null,'\0',sizeof(char)*ISEG_MSG_SIZE);
    memset(reply,'\0',sizeof(char)*ISEG_MSG_SIZE);
    memset(error,'\0',sizeof(char)*ISEG_MSG_SIZE);

    sprintf(msg, "T%d", channel);
    if(ISEG_T2DP66_write(serial_port, msg, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    // ISEG replies with the command you sent, new line, then the response. So, we need to read twice
    if(ISEG_T2DP66_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    memset(iseg_reply,'\0',sizeof(char)*ISEG_MSG_SIZE);
    if(ISEG_T2DP66_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}

	if(iseg_reply[0] == '0'|| '1'){
    	sprintf(reply, "Trip setting: %s", iseg_reply);
	}
	else{strcpy(reply, null);sprintf(error, "Returned message unreadable");return ISEG_COM_ERROR;}

    return ISEG_OK;
}

/// Gets the present status of the ISEG HV power supply
/// \param serial_port file descriptor returned from ISEG_T2DP66_connect
/// \param channel 1 or 2
/// \param reply holds response or message
/// \return status code ISEG_STATUS_XX
int ISEG_T2DP66_get_status(const int serial_port, const int channel, char* reply, char* error)
{
    char msg[ISEG_MSG_SIZE];
    char iseg_reply[ISEG_MSG_SIZE];
    char err[ISEG_MSG_SIZE];

    memset(reply,'\0',sizeof(char)*ISEG_MSG_SIZE);
    memset(error,'\0',sizeof(char)*ISEG_MSG_SIZE);

    sprintf(msg, "S%d", channel);

    if(ISEG_T2DP66_write(serial_port, msg, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    // ISEG replies with the command you sent, new line, then the response. So, we need to read twice
    if(ISEG_T2DP66_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}

    memset(iseg_reply,'\0',sizeof(char)*ISEG_MSG_SIZE);
    if(ISEG_T2DP66_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}

    if(ISEG_T2DP66_get_status_message(iseg_reply,reply, error)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}

    return ISEG_OK;
}

/// set target voltage on ISEG HV power supply
/// \param serial_port file descriptor returned from ISEG_T2DP66_connect
/// \param channel 1 or 2
/// \param reply holds response or message
/// \param target_voltage desired maximum voltage to hold at
/// \return ISEG_COM_ERROR on error or ISEG_OK on all clear
int ISEG_T2DP66_set_voltage(const int serial_port, const int channel, double target_voltage, char* error)
{
    char msg[ISEG_MSG_SIZE];
    char iseg_reply[ISEG_MSG_SIZE];
    char err[ISEG_MSG_SIZE];

    memset(error,'\0',sizeof(char)*ISEG_MSG_SIZE);

    sprintf(msg, "D%d=%f",channel, fabs(target_voltage));

    if(ISEG_T2DP66_write(serial_port, msg, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}

    if(ISEG_T2DP66_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}

    return ISEG_OK;
}

int ISEG_T2DP66_get_voltage(const int serial_port, const int channel, char* reply, double* voltage, char* error)
{
    char msg[ISEG_MSG_SIZE];
    char iseg_reply[ISEG_MSG_SIZE];
    char err[ISEG_MSG_SIZE];
	char null[ISEG_MSG_SIZE];

	memset(null,'\0',sizeof(char)*ISEG_MSG_SIZE);

    long mantissa = 0, signed_exponent = 0;

    memset(reply,'\0',sizeof(char)*ISEG_MSG_SIZE);
    memset(error,'\0',sizeof(char)*ISEG_MSG_SIZE);

    sprintf(msg, "D%d", channel);

    if(ISEG_T2DP66_write(serial_port, msg, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;} //todo: change com error to standard failure
    // ISEG replies with the command you sent, new line, then the response. So, we need to read twice
    if(ISEG_T2DP66_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}

	if(iseg_reply[0] == 'D'){
    	strcpy(reply, iseg_reply);
	}
	else{strcpy(reply, null);sprintf(error, "Returned message unreadable");return ISEG_COM_ERROR;}

    memset(iseg_reply,'\0',sizeof(char)*ISEG_MSG_SIZE);
    if(ISEG_T2DP66_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}

    // parse voltage from reply string
    char* pEnd;
    mantissa = strtol(iseg_reply, &pEnd,10);
    signed_exponent = strtol(pEnd, &pEnd,10);
    *voltage = (double)mantissa * pow(10, (double)signed_exponent);

    return ISEG_OK;
}

int ISEG_T2DP66_set_current(const int serial_port, const int channel,  double current, char* error)
{
    char msg[ISEG_MSG_SIZE];
    char iseg_reply[ISEG_MSG_SIZE];
    char err[ISEG_MSG_SIZE];

    memset(error,'\0',sizeof(char)*ISEG_MSG_SIZE);

    sprintf(msg, "C%d=%f", channel, current* pow(10, -3)); //convert to mA

    if(ISEG_T2DP66_write(serial_port, msg, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    // ISEG replies with the command you sent, new line, then the response. So, we need to read twice
    if(ISEG_T2DP66_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}

    //memset(iseg_reply,'\0',sizeof(char)*ISEG_MSG_SIZE);
    //if(ISEG_T2DP66_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    return ISEG_OK;
}

int ISEG_T2DP66_get_current(const int serial_port, const int channel, char* reply, double* current, char* error)
{
    char msg[ISEG_MSG_SIZE];
    char iseg_reply[ISEG_MSG_SIZE];
    char err[ISEG_MSG_SIZE];
	char null[ISEG_MSG_SIZE];

	memset(null,'\0',sizeof(char)*ISEG_MSG_SIZE);

    long mantissa = 0, signed_exponent = 0;

    memset(reply,'\0',sizeof(char)*ISEG_MSG_SIZE);
    memset(error,'\0',sizeof(char)*ISEG_MSG_SIZE);

    sprintf(msg, "C%d", channel);

    if(ISEG_T2DP66_write(serial_port, msg, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    // ISEG replies with the command you sent, new line, then the response. So, we need to read twice
    if(ISEG_T2DP66_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}

	if(iseg_reply[0] == 'C'){
    	strcpy(reply, iseg_reply);
	}
	else{strcpy(reply, null);sprintf(error, "Returned message unreadable");return ISEG_COM_ERROR;}

    memset(iseg_reply,'\0',sizeof(char)*ISEG_MSG_SIZE);
    if(ISEG_T2DP66_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}

    // parse voltage from reply string
    char* pEnd;
    mantissa = strtol(iseg_reply, &pEnd,10);
    signed_exponent = strtol(pEnd, &pEnd,10);
    *current = (double)mantissa * pow(10, (double)signed_exponent); //convert to mA

    return ISEG_OK;
}

/// Measures the voltage for selected channel on the ISEG HV power supply
/// \param serial_port file descriptor returned from ISEG_T2DP66_connect
/// \param channel channel to measure
/// \param reply holds error message or other info
/// \param voltage measured voltage is returned through pointer
/// \return ISEG_COM_ERROR on error or ISEG_OK on all clear
int ISEG_T2DP66_measure_voltage(const int serial_port, const int channel, double* voltage, char* error)
{
    char msg[ISEG_MSG_SIZE];
    char iseg_reply[ISEG_MSG_SIZE];
    char err[ISEG_MSG_SIZE];

    int mantissa = 0, signed_exponent = 0;

    memset(error,'\0',sizeof(char)*ISEG_MSG_SIZE);

    sprintf(msg, "U%d", channel);

    if(ISEG_T2DP66_write(serial_port, msg, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    // ISEG replies with the command you sent, new line, then the response. So, we need to read twice
    if(ISEG_T2DP66_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}

	if(iseg_reply[0] != 'U')
	{
		sprintf(error, "Returned message unreadable");
		return ISEG_COM_ERROR;
	}

    memset(iseg_reply,'\0',sizeof(char)*ISEG_MSG_SIZE);
    if(ISEG_T2DP66_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}

    // parse voltage from reply string
    char* pEnd;
    mantissa = strtol(iseg_reply, &pEnd,10);
    signed_exponent = strtol(pEnd, &pEnd,10);
    *voltage = (double)mantissa * pow(10, (double)signed_exponent);

    return ISEG_OK;
}

/// Measures the current for selected channel on the ISEG HV power supply
/// \param serial_port file descriptor returned from ISEG_T2DP66_connect
/// \param channel channel to measure
/// \param reply holds error message or other info
/// \param current measured current is returned through pointer
/// \return ISEG_COM_ERROR on error or ISEG_OK on all clear
int ISEG_T2DP66_measure_current(const int serial_port, const int channel, double* current, char* error)
{
    char msg[ISEG_MSG_SIZE];
    char iseg_reply[ISEG_MSG_SIZE];
    char err[ISEG_MSG_SIZE];

    long mantissa = 0, signed_exponent = 0;

    memset(error,'\0',sizeof(char)*ISEG_MSG_SIZE);

    sprintf(msg, "I%d", channel);

    if(ISEG_T2DP66_write(serial_port, msg, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    // ISEG replies with the command you sent, new line, then the response. So, we need to read twice
    if(ISEG_T2DP66_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}

	if(iseg_reply[0] != 'I')
	{
		sprintf(error, "Returned message unreadable");
		return ISEG_COM_ERROR;
	}

    memset(error,'\0',sizeof(char)*ISEG_MSG_SIZE);
    if(ISEG_T2DP66_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}

    // parse current from reply string
    char* pEnd;
    mantissa = strtol(iseg_reply, &pEnd,10);
    signed_exponent = strtol(pEnd, &pEnd,10);
    *current = (double)mantissa * pow(10, (double)signed_exponent);

    return ISEG_OK;
}

int ISEG_T2DP66_set_single_echo(const int serial_port, const int channel, char* error)
{
    char msg[ISEG_MSG_SIZE];
    char iseg_reply[ISEG_MSG_SIZE];
    char err[ISEG_MSG_SIZE];

    memset(error,'\0',sizeof(char)*ISEG_MSG_SIZE);

    sprintf(msg, "E%d=1", channel);
    if(ISEG_T2DP66_write(serial_port, msg, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    // ISEG replies with the command you sent, new line, then the response. So, we need to read twice
    if(ISEG_T2DP66_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    //memset(iseg_reply,'\0',sizeof(char)*ISEG_MSG_SIZE);
    //if(ISEG_T2DP66_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    return ISEG_OK;
}
