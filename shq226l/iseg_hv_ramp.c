//
// Created by neutron on 2/14/20.
//

#include "iseg_shq226l.h"
#include "stdio.h"
#include <math.h>

void print_arg_nan(char* argv)
{
    printf("\n\n  Argument %s was not a number         \n\n", argv);
}

void print_usage()
{
    printf("\n +---------------ISEG HV RAMP USAGE---------------+");
    printf("\n $>./iseg_hv_ramp -v <target_voltage> -r <ramp_rate>");
    printf("\n target_voltage and ramp rate must be positive.     \n\n");
}
/*int parse_commands(int argc, char* argv[], double* target_voltage, int* ramp_rate)
{
    int argi;
    char* pEnd;
    char cset[] = "1234567890";

    if(argc<5)
    {
        print_usage();
        return -1;
    }
    else{
        for(argi=1; argi<argc; argi+=2)
        {
            if(strcmp(argv[argi],"-v") == 0)
            {
                if(strspn(argv[argi+1],cset) == strlen(argv[argi+1]))
                {*target_voltage = strtod(argv[argi+1],&pEnd);}
                else
                {print_arg_nan(argv[argi+1]); return -1;}
            }
            if(strcmp(argv[argi],"-r") == 0)
            {
                if(strspn(argv[argi+1],cset) == strlen(argv[argi+1]))
                {*ramp_rate = strtol(argv[argi+1],&pEnd,10);}
                else
                {print_arg_nan(argv[argi+1]);return -1;}
            }
            if(strcmp(argv[argi],"-h") == 0)
            {print_arg_nan(argv[argi+1]);return -1;}
        }
    }
    return 0;
}*/

int main(int argc, char* argv[])
{
    printf("\n +----------------------------------+");
    printf("\n |    ISEG SHQ226L HV PowerSupply   |");
    printf("\n +----------------------------------+\n\n");
    int argi;
    char* pEnd;
    double target_voltage = 100;
    int ramp_rate = 1;
    char error[ISEG_MSG_SIZE];
    //printf("%d", argc);
    //if(parse_commands(argc, argv, &target_voltage, &ramp_rate)<0){return 0;};

    //if(target_voltage < 0 || ramp_rate < 0)
    //{printf("Target voltage or ramp rate negative.");print_usage();return 0;}

    /*------ Opening the Serial Port -----*/
    int fd = ISEG_SHQ226L_connect("/dev/ttyMXUSB0", error);

    if(fd == -1){printf("Connection failure %s\n",error); return -1;}

    char reply[ISEG_MSG_SIZE];
    double measured_voltage = 0.;
    double set_voltage = 0.;
    printf("ISEG SHQ226L FD: %d\n", fd);

    if(ISEG_SHQ226L_identify(fd, reply, error)){printf("%s\n", error); return -1;}
    else{printf("ID: %s\n", reply);}

    if(ISEG_SHQ226L_get_status(fd, 1, reply, error)){printf("%s\n", error); return -1;}
    printf("Status: %s\n", reply);

    if(ISEG_SHQ226L_set_voltage(fd, 1, reply, target_voltage, error)){printf("%s\n", error); return -1;}

    if(ISEG_SHQ226L_set_ramp_rate(fd, 1, reply, ramp_rate, error)){printf("%s\n", error); return -1;}

    if(ISEG_SHQ226L_get_voltage(fd, 1, reply, &set_voltage, error)){printf("%s\n", error); return -1;}

    printf("Target Voltage: %lf\n", target_voltage);
    printf("Set Voltage: %lf\n", set_voltage);
    printf("Starting HV Ramp\n");

    if(ISEG_SHQ226L_start_ramp(fd, 1, reply, error)){printf("%s\n", error); return -1;}

    printf("Start Ramp reply: %s\n", reply);

    if(ISEG_SHQ226L_measure_voltage(fd, 1, reply, &measured_voltage, error)){printf("%s\n", error); return -1;}

    while (fabs(fabs(measured_voltage) - target_voltage) > 1.5)
    {
        if(ISEG_SHQ226L_measure_voltage(fd, 1, reply, &measured_voltage, error)){printf("%s\n", error); return -1;}
        printf("Measured V: %lf\r", measured_voltage);
        fflush(stdout);
    }
    printf("\n");
    target_voltage = 0.;
    if(ISEG_SHQ226L_set_voltage(fd, 1, reply, target_voltage, error)){printf("%s\n", error); return -1;}
    if(ISEG_SHQ226L_get_voltage(fd, 1, reply, &set_voltage, error)){printf("%s\n", error); return -1;}
    printf("Set V: %lf\n",set_voltage);
    if(ISEG_SHQ226L_start_ramp(fd, 1, reply, error)){printf("%s\n", error); return -1;}
    printf("Start Ramp reply: %s\n",reply);

    while(fabs(fabs(measured_voltage)-target_voltage) > 1.0)
    {
        if(ISEG_SHQ226L_measure_voltage(fd, 1, reply, &measured_voltage, error)){printf("%s\n", error); return -1;}
        printf("Measured V: %lf\r", measured_voltage);
        fflush(stdout);
    }
    printf("\n");
    if(ISEG_SHQ226L_disconnect(fd, error)){printf("%s\n", error); return -1;}
    return ISEG_OK;
}
