#include "iseg_shq226l.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <termios.h>
#include <math.h>

struct termios old_tty_opt;


/// connect to ISEG HV powersupply connected on serial com port
/// \param port_id string of path to serial file
/// \return serial_port file descriptor
int ISEG_SHQ226L_connect(const char* port_id, char* error)
{
    memset(error, '\0', ISEG_MSG_SIZE* sizeof(char));
    int serial_port = open(port_id, (O_RDWR|O_SYNC|O_NOCTTY) );
    if(serial_port<0){return ISEG_CONNECT_FAIL;}
    struct termios new_tty_opt;
    tcgetattr(serial_port, &old_tty_opt); /* save current serial port settings */
    bzero(&new_tty_opt, sizeof(new_tty_opt)); /* clear struct for new port settings */

    new_tty_opt.c_cflag &= ~PARENB;
    new_tty_opt.c_cflag &= ~CSTOPB;
    new_tty_opt.c_cflag &= ~CSIZE;
    new_tty_opt.c_cflag |= CS8|CLOCAL|CREAD;
    new_tty_opt.c_lflag |= ICANON;
    new_tty_opt.c_lflag &= ~(ECHO|ECHOE);
    new_tty_opt.c_iflag |= (IXON|IXOFF|IXANY);
    new_tty_opt.c_oflag |= OPOST; //raw: new_tty_opt.c_oflag &= ~OPOST;
    new_tty_opt.c_cc[VTIME] = 1;//delay of 3 ms in send data
    //new_tty_opt.c_iflag |= IGNBRK; //input options
    //new_tty_opt.c_cflag |= CS8 | CLOCAL | CREAD; //control options
    //new_tty_opt.c_iflag &= ~(INLCR | IGNCR | ICRNL); //repeated
    //new_tty_opt.c_oflag &= ~(ONLCR | OCRNL | OPOST); //output options
    //new_tty_opt.c_lflag |= ICANON;  /* Canonical mode*/  //line options
    //new_tty_opt.c_lflag &= ~(ECHO | ECHOE | ECHONL | IEXTEN);

//    new_tty_opt.c_cc[VMIN] = 1;
//    new_tty_opt.c_cc[VTIME] = 1;

    cfsetispeed(&new_tty_opt, B9600);
    cfsetospeed(&new_tty_opt, B9600);
    tcflush(serial_port, TCIOFLUSH);

    if(tcsetattr(serial_port, TCSANOW, &new_tty_opt) != 0)
    {
        sprintf(error, "ISEG error opening device, errno: %s", strerror(errno));
        return ISEG_CONNECT_FAIL;
    }
    return serial_port;
}

/// disconnect from ISEG HV power supply, freeing serial port file descriptor, reset old serial attr
/// \param serial_port file descriptor returned from ISEG_SHQ226L_connect
/// \return 0 on success or errno
int ISEG_SHQ226L_disconnect(const int serial_port, char* error)
{
    memset(error, '\0', ISEG_MSG_SIZE* sizeof(char));
    if(tcsetattr(serial_port, TCSANOW, &old_tty_opt) != 0)
    {
        sprintf(error, "ISEG error closing device, errno: %s", strerror(errno));
        return ISEG_CONNECT_FAIL;
    }
    return close(serial_port);
}

/// For internal use, write function for ISEG HV
/// \param serial_port file descriptor returned from ISEG_SHQ226L_connect
/// \param message command to send to ISEG HV power supply
/// \return ISEG_COM_ERROR on error or ISEG_OK on all clear
int ISEG_SHQ226L_write(const int serial_port, char* message, char* error)
{
    memset(error, '\0', ISEG_MSG_SIZE* sizeof(char));
    char iseg_msg[ISEG_MSG_SIZE];
    memset(iseg_msg,'\0',sizeof(char)*ISEG_MSG_SIZE);
    sprintf(iseg_msg, "%s\r\n",message);
    int bytes_to_write = sizeof(char)*strlen(iseg_msg);
    int bytes_written = write(serial_port, iseg_msg, bytes_to_write);
    if(bytes_to_write != bytes_written){sprintf(error, "ISEG bytes to write does not match bytes written"); return ISEG_COM_ERROR;}
    return ISEG_OK;
}

/// For internal use, read function for ISEG HV
/// \param serial_port file descriptor returned from ISEG_SHQ226L_connect
/// \param reply holds response or error message
/// \return ISEG_COM_ERROR on error or ISEG_OK on all clear
int ISEG_SHQ226L_read(const int serial_port, char* reply, char* error)
{
    memset(reply, '\0', ISEG_MSG_SIZE* sizeof(char));
    memset(error, '\0', ISEG_MSG_SIZE* sizeof(char));
    char iseg_reply[ISEG_MSG_SIZE];
    memset(iseg_reply,'\0',sizeof(char)*ISEG_MSG_SIZE);
    int bytes_read = read(serial_port, iseg_reply, sizeof(char)*ISEG_MSG_SIZE);
    // check for error on the reply
    iseg_reply[strcspn(iseg_reply, "\r\n")] = '\0'; // remove carriage return, new line
    if((strncmp(iseg_reply,"?",1) == 0) || bytes_read == 0)
    {
        char error_message[ISEG_MSG_SIZE];
        if(ISEG_SHQ226L_get_error_message(iseg_reply,error_message))
        {
            sprintf(error, "ISEG failure to get error message in read");
            return ISEG_COM_ERROR;
        }
        sprintf(error, "ISEG error in read: %s", error_message);
        return ISEG_COM_ERROR;
    }
    else
    {
        memset(reply,'\0',sizeof(char)*ISEG_MSG_SIZE);
        strcpy(reply,iseg_reply);
    }
    return ISEG_OK;
}


/// Gets the error message as written in the ISEG manual
/// \param error_code string error code returned from device
/// \param error_message string error message as in manual
/// \return ISEG_COM_ERROR on error or ISEG_OK on all clear
int ISEG_SHQ226L_get_error_message(char* error_code, char* reply)
{
    memset(reply, '\0', ISEG_MSG_SIZE* sizeof(char));
    unsigned int code = 0x0U;
    char error_message[ISEG_MSG_SIZE];
    code = strcmp(error_code,"????") == 0x0U ?          ISEG_ERROR_SYNTAX   : code;
    code = strcmp(error_code,"?WCN") == 0x0U ?         ISEG_ERROR_WCN      : code;
    code = strcmp(error_code,"?TOT") == 0x0U ?          ISEG_ERROR_TOT      : code;
    code = strncmp(error_code,"? UMAX=",6) == 0x0U ?    ISEG_ERROR_UMAX     : code;
    code = strcmp(error_code,"") == 0x0U ?              ISEG_ERROR_UKN      : code;
    memset(error_message,'\0',ISEG_MSG_SIZE*sizeof(char));
    double volt_limit=0.;

    switch(code)
    {
        case ISEG_ERROR_SYNTAX:
            sprintf(reply,"Syntax error");
            break;
        case ISEG_ERROR_WCN:
            sprintf(reply,"Wrong channel number");
            break;
        case ISEG_ERROR_TOT:
            sprintf(reply,"Timeout error");
            break;
        case ISEG_ERROR_UMAX:
            sscanf(error_code,"%*s%lf\r\n",&volt_limit);
            sprintf(reply,"Set voltage exceeds voltage limit %f",volt_limit);
            break;
        default:
            sprintf(reply,"Unknown error code: %s", error_code);
            break;
    }
    return ISEG_OK;
}

/// Gets the status message as written in the ISEG manual
/// \param error_code string error code returned from device
/// \param error_message string error message as in manual
/// \return ISEG_STATUS_XX
int ISEG_SHQ226L_get_status_message(char* status_code, char* reply)
{
    memset(reply, '\0', ISEG_MSG_SIZE* sizeof(char));
    unsigned int code = 0x0U;
    char channel_str[ISEG_MSG_SIZE];
    memset(channel_str, '\0', ISEG_MSG_SIZE* sizeof(char));
	int i =0;
    for(i=3;i<strlen(status_code);i++)
    {
        strncat(channel_str, &status_code[i], 1);
    }
    code = strcmp(channel_str,"ON") == 0x0U    ? ISEG_STATUS_ON : code;
    code = strcmp(channel_str,"OFF") == 0x0U   ? ISEG_STATUS_OFF : code;
    code = strcmp(channel_str,"MAN") == 0x0U   ? ISEG_STATUS_MAN : code;
    code = strcmp(channel_str,"ERR") == 0x0U   ? ISEG_STATUS_ERR : code;
    code = strcmp(channel_str,"INH") == 0x0U   ? ISEG_STATUS_INH : code;
    code = strcmp(channel_str,"QUA") == 0x0U   ? ISEG_STATUS_QUA : code;
    code = strcmp(channel_str,"L2H") == 0x0U   ? ISEG_STATUS_L2H : code;
    code = strcmp(channel_str,"H2L") == 0x0U   ? ISEG_STATUS_H2L : code;
    code = strcmp(channel_str,"LAS") == 0x0U   ? ISEG_STATUS_LAS : code;
    code = strcmp(channel_str,"TRP") == 0x0U   ? ISEG_STATUS_TRP : code;
    code = strcmp(channel_str,"") == 0x0U      ? ISEG_STATUS_UNK : code;
    switch(code)
    {
        case ISEG_STATUS_ON:
            sprintf(reply,"Output voltage according to set voltage");
            break;
        case ISEG_STATUS_OFF:
            sprintf(reply,"Channel front panel switch off");
            break;
        case ISEG_STATUS_MAN:
            sprintf(reply,"Channel is on, set to manual mode");
            break;
        case ISEG_STATUS_ERR:
            sprintf(reply,"V max or I max is or was exceeded");
            break;
        case ISEG_STATUS_INH:
            sprintf(reply,"Inhibit signal is or was active");
            break;
        case ISEG_STATUS_QUA:
            sprintf(reply,"Quality of output voltage not given at present");
            break;
        case ISEG_STATUS_L2H:
            sprintf(reply,"Output voltage increasing");
            break;
        case ISEG_STATUS_H2L:
            sprintf(reply,"Output voltage decreasing");
            break;
        case ISEG_STATUS_LAS:
            sprintf(reply,"Look at Status (only after G-command)");
            break;
        case ISEG_STATUS_TRP:
            sprintf(reply,"Current trip was active");
            break;
        default:
            sprintf(reply,"Unknown Status Code 0x%4x", code);
            break;
    }
    return ISEG_OK;
}

/// Gets ISEG HV power supply device infomation: Serial number; software release; V out max ; I out max
/// \param serial_port file descriptor returned from ISEG_SHQ226L_connect
/// \param reply holds response or message
/// \return ISEG_COM_ERROR on error or ISEG_OK on all clear
int ISEG_SHQ226L_identify(const int serial_port, char* reply, char* error)
{
    char msg[ISEG_MSG_SIZE];
    char iseg_reply[ISEG_MSG_SIZE];
    char err[ISEG_MSG_SIZE];

    memset(reply,'\0',sizeof(char)*ISEG_MSG_SIZE);
    memset(error,'\0',sizeof(char)*ISEG_MSG_SIZE);

    sprintf(msg, "#");
    if(ISEG_SHQ226L_write(serial_port, msg, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    // ISEG replies with the command you sent, new line, then the response. So, we need to read twice
    if(ISEG_SHQ226L_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    memset(iseg_reply,'\0',sizeof(char)*ISEG_MSG_SIZE);
    if(ISEG_SHQ226L_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    strcpy(reply, iseg_reply);
    return ISEG_OK;
}

/// Gets the present status of the ISEG HV power supply
/// \param serial_port file descriptor returned from ISEG_SHQ226L_connect
/// \param channel 1 or 2
/// \param reply holds response or message
/// \return status code ISEG_STATUS_XX
int ISEG_SHQ226L_get_status(const int serial_port, const int channel, char* reply, char* error)
{
    char msg[ISEG_MSG_SIZE];
    char iseg_reply[ISEG_MSG_SIZE];
    char err[ISEG_MSG_SIZE];

    memset(reply,'\0',sizeof(char)*ISEG_MSG_SIZE);
    memset(error,'\0',sizeof(char)*ISEG_MSG_SIZE);

    sprintf(msg, "S%d", channel);
    if(ISEG_SHQ226L_write(serial_port, msg, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    // ISEG replies with the command you sent, new line, then the response. So, we need to read twice
    if(ISEG_SHQ226L_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    memset(iseg_reply,'\0',sizeof(char)*ISEG_MSG_SIZE);
    if(ISEG_SHQ226L_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}

    ISEG_SHQ226L_get_status_message(iseg_reply,reply);

    return ISEG_OK;
}


/// set target voltage on ISEG HV power supply
/// \param serial_port file descriptor returned from ISEG_SHQ226L_connect
/// \param channel 1 or 2
/// \param reply holds response or message
/// \param target_voltage desired maximum voltage to hold at
/// \return ISEG_COM_ERROR on error or ISEG_OK on all clear
int ISEG_SHQ226L_set_voltage(const int serial_port, const int channel, char* reply, const double target_voltage, char* error)
{
    char msg[ISEG_MSG_SIZE];
    char iseg_reply[ISEG_MSG_SIZE];
    char empty_reply[ISEG_MSG_SIZE];
    char err[ISEG_MSG_SIZE];

    memset(reply,'\0',sizeof(char)*ISEG_MSG_SIZE);
    memset(error,'\0',sizeof(char)*ISEG_MSG_SIZE);

    sprintf(msg, "D%d=%0.2f",channel, fabs(target_voltage));
    if(ISEG_SHQ226L_write(serial_port, msg, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    // ISEG replies with the command you sent, new line, then the response. So, we need to read twice
    if(ISEG_SHQ226L_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}

    if(ISEG_SHQ226L_read(serial_port, empty_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}

    strcpy(reply, iseg_reply);
    return ISEG_OK;
}

/// set voltage ramp rate in volts per second on ISEG HV power supply
/// \param serial_port file descriptor returned from ISEG_SHQ226L_connect
/// \param channel 1 or 2
/// \param reply holds response or message
/// \param ramp_rate volts per second
/// \return ISEG_COM_ERROR on error or ISEG_OK on all clear
int ISEG_SHQ226L_set_ramp_rate(const int serial_port, const int channel, char* reply, const int ramp_rate, char* error)
{
    char msg[ISEG_MSG_SIZE];
    char iseg_reply[ISEG_MSG_SIZE];
    char empty_reply[ISEG_MSG_SIZE];
    char err[ISEG_MSG_SIZE];

    memset(reply, '\0', sizeof(char) * ISEG_MSG_SIZE);
    memset(error, '\0', sizeof(char) * ISEG_MSG_SIZE);

    sprintf(msg, "V%d=%d", channel, ramp_rate);
    if (ISEG_SHQ226L_write(serial_port, msg, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    // ISEG replies with the command you sent, new line, then the response. So, we need to read twice
    if (ISEG_SHQ226L_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}

    if (ISEG_SHQ226L_read(serial_port, empty_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    strcpy(reply, iseg_reply);
    return ISEG_OK;
}

/// Gets the currently set ramp rate in volts per second on the ISEG HV power supply
/// \param serial_port file descriptor returned from ISEG_SHQ226L_connect
/// \param channel 1 or 2
/// \param reply holds response or message
/// \param ramp_rate volts per second
/// \return ISEG_COM_ERROR on error or ISEG_OK on all clear
int ISEG_SHQ226L_get_ramp_rate(const int serial_port, const int channel, char* reply, int* ramp_rate, char* error)
{
    char msg[ISEG_MSG_SIZE];
    char iseg_reply[ISEG_MSG_SIZE];
    char empty_reply[ISEG_MSG_SIZE];
    char err[ISEG_MSG_SIZE];

    memset(error,'\0',sizeof(char)*ISEG_MSG_SIZE);
    memset(reply,'\0',sizeof(char)*ISEG_MSG_SIZE);

    sprintf(msg, "D%d", channel);
    if(ISEG_SHQ226L_write(serial_port, msg, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    // ISEG replies with the command you sent, new line, then the response. So, we need to read twice
    if(ISEG_SHQ226L_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}

    if(ISEG_SHQ226L_read(serial_port, empty_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}

    strcpy(reply, iseg_reply);
    // parse voltage from reply string
    char* pEnd;
    *ramp_rate = (int)strtol(iseg_reply, &pEnd,10);

    return ISEG_OK;
}

/// start ramping voltage from current value to the voltage defined by ISEG_SHQ226L_set_voltage at rate set by ISEG_SHQ226L_set_ramp_rate
/// \param serial_port file descriptor returned from ISEG_SHQ226L_connect
/// \param channel 1 or 2
/// \param reply holds response or message
/// \return ISEG_COM_ERROR on error or ISEG_OK on all clear
int ISEG_SHQ226L_start_ramp(const int serial_port, const int channel, char* reply, char* error)
{
    char msg[ISEG_MSG_SIZE];
    char iseg_reply[ISEG_MSG_SIZE];
    char err[ISEG_MSG_SIZE];

    memset(reply,'\0',sizeof(char)*ISEG_MSG_SIZE);
    memset(error,'\0',sizeof(char)*ISEG_MSG_SIZE);

    sprintf(msg, "G%d",channel);
    if(ISEG_SHQ226L_write(serial_port, msg, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    // ISEG replies with the command you sent, new line, then the response. So, we need to read twice
    if(ISEG_SHQ226L_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    memset(iseg_reply,'\0',sizeof(char)*ISEG_MSG_SIZE);
    if(ISEG_SHQ226L_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    ISEG_SHQ226L_get_status_message(iseg_reply,reply);
    return ISEG_OK;
}

/// Gets the currently set voltage on the ISEG HV power supply
/// \param serial_port file descriptor returned from ISEG_SHQ226L_connect
/// \param channel channel to measure (1 or 2)
/// \param reply holds error message or other info
/// \param voltage measured voltage is returned through pointer
/// \return ISEG_COM_ERROR on error or ISEG_OK on all clear
int ISEG_SHQ226L_get_voltage(const int serial_port, const int channel, char* reply, double* voltage, char* error)
{
    char msg[ISEG_MSG_SIZE];
    char iseg_reply[ISEG_MSG_SIZE];
    char err[ISEG_MSG_SIZE];

    long mantissa = 0, signed_exponent = 0;

    memset(reply,'\0',sizeof(char)*ISEG_MSG_SIZE);
    memset(error,'\0',sizeof(char)*ISEG_MSG_SIZE);

    sprintf(msg, "D%d", channel);
    if(ISEG_SHQ226L_write(serial_port, msg, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    // ISEG replies with the command you sent, new line, then the response. So, we need to read twice
    if(ISEG_SHQ226L_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    memset(iseg_reply,'\0',sizeof(char)*ISEG_MSG_SIZE);
    if(ISEG_SHQ226L_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}

    strcpy(reply, iseg_reply);
    // parse voltage from reply string
    char* pEnd;
    mantissa = strtol(iseg_reply, &pEnd,10);
    signed_exponent = strtol(pEnd, &pEnd,10);
    //*voltage = (double)mantissa * pow(10, (double)signed_exponent);

    return ISEG_OK;
}

/// Measures the voltage for selected channel on the ISEG HV power supply
/// \param serial_port file descriptor returned from ISEG_SHQ226L_connect
/// \param channel channel to measure
/// \param reply holds error message or other info
/// \param voltage measured voltage is returned through pointer
/// \return ISEG_COM_ERROR on error or ISEG_OK on all clear
int ISEG_SHQ226L_measure_voltage(const int serial_port, const int channel, char* reply, double* voltage, char* error)
{
    char msg[ISEG_MSG_SIZE];
    char iseg_reply[ISEG_MSG_SIZE];
    char err[ISEG_MSG_SIZE];

    int mantissa = 0, signed_exponent = 0;

    memset(reply,'\0',sizeof(char)*ISEG_MSG_SIZE);
    memset(error,'\0',sizeof(char)*ISEG_MSG_SIZE);

    sprintf(msg, "U%d", channel);
    if(ISEG_SHQ226L_write(serial_port, msg, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    // ISEG replies with the command you sent, new line, then the response. So, we need to read twice
    if(ISEG_SHQ226L_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    memset(iseg_reply,'\0',sizeof(char)*ISEG_MSG_SIZE);
    if(ISEG_SHQ226L_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}

    strcpy(reply, iseg_reply);
    // parse voltage from reply string
    char* pEnd;
    mantissa = strtol(iseg_reply, &pEnd,10);
    signed_exponent = strtol(pEnd, &pEnd,10);
    //*voltage = (double)mantissa * pow(10, (double)signed_exponent);

    return ISEG_OK;
}

/// Measures the current for selected channel on the ISEG HV power supply
/// \param serial_port file descriptor returned from ISEG_SHQ226L_connect
/// \param channel channel to measure
/// \param reply holds error message or other info
/// \param current measured current is returned through pointer
/// \return ISEG_COM_ERROR on error or ISEG_OK on all clear
int ISEG_SHQ226L_measure_current(const int serial_port, const int channel, char* reply, double* current, char* error)
{
    char msg[ISEG_MSG_SIZE];
    char iseg_reply[ISEG_MSG_SIZE];
    char err[ISEG_MSG_SIZE];

    long mantissa = 0, signed_exponent = 0;

    memset(reply,'\0',sizeof(char)*ISEG_MSG_SIZE);
    memset(error,'\0',sizeof(char)*ISEG_MSG_SIZE);

    sprintf(msg, "I%d", channel);
    if(ISEG_SHQ226L_write(serial_port, msg, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    // ISEG replies with the command you sent, new line, then the response. So, we need to read twice
    if(ISEG_SHQ226L_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    memset(error,'\0',sizeof(char)*ISEG_MSG_SIZE);
    if(ISEG_SHQ226L_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}

    strcpy(reply, iseg_reply);
    // parse current from reply string
    char* pEnd;
    mantissa = strtol(iseg_reply, &pEnd,10);
    signed_exponent = strtol(pEnd, &pEnd,10);
    //*current = (double)mantissa * pow(10, (double)signed_exponent);

    return ISEG_OK;
}

/// Gets the presently set voltage limit
/// \param serial_port file descriptor returned from ISEG_SHQ226L_connect
/// \param channel 1 or 2
/// \param reply holds error message or other info
/// \param limit is returned through pointer in % of V out max
/// \return ISEG_COM_ERROR on error or ISEG_OK on all clear
int ISEG_SHQ226L_get_voltage_limit(const int serial_port, const int channel, char* reply, double* limit, char* error)
{
    char msg[ISEG_MSG_SIZE];
    char iseg_reply[ISEG_MSG_SIZE];
    char err[ISEG_MSG_SIZE];

    long mantissa = 0, signed_exponent = 0;

    memset(reply,'\0',sizeof(char)*ISEG_MSG_SIZE);
    memset(error,'\0',sizeof(char)*ISEG_MSG_SIZE);

    sprintf(msg, "M%d", channel);
    if(ISEG_SHQ226L_write(serial_port, msg, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    // ISEG replies with the command you sent, new line, then the response. So, we need to read twice
    if(ISEG_SHQ226L_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    memset(iseg_reply,'\0',sizeof(char)*ISEG_MSG_SIZE);
    if(ISEG_SHQ226L_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}

    strcpy(reply, iseg_reply);
    // parse limit from reply string
    char* pEnd;
    *limit = strtod(iseg_reply, &pEnd);

    return ISEG_OK;
}

/// Gets the presently set current limit
/// \param serial_port file descriptor returned from ISEG_SHQ226L_connect
/// \param channel 1 or 2
/// \param reply holds error message or other info
/// \param limit is returned through pointer in % of I out max
/// \return ISEG_COM_ERROR on error or ISEG_OK on all clear
int ISEG_SHQ226L_get_current_limit(const int serial_port, const int channel, char* reply, double* limit, char* error)
{
    char msg[ISEG_MSG_SIZE];
    char iseg_reply[ISEG_MSG_SIZE];
    char err[ISEG_MSG_SIZE];

    long mantissa = 0, signed_exponent = 0;

    memset(error,'\0',sizeof(char)*ISEG_MSG_SIZE);
    memset(reply,'\0',sizeof(char)*ISEG_MSG_SIZE);

    sprintf(msg, "N%d", channel);
    if(ISEG_SHQ226L_write(serial_port, msg, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    // ISEG replies with the command you sent, new line, then the response. So, we need to read twice
    if(ISEG_SHQ226L_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    memset(iseg_reply,'\0',sizeof(char)*ISEG_MSG_SIZE);
    if(ISEG_SHQ226L_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}

    strcpy(reply, iseg_reply);
    // parse current limit from reply string
    char* pEnd;
    mantissa = strtol(iseg_reply, &pEnd,10);
    signed_exponent = strtol(pEnd, &pEnd,10);
    //*limit = (double)mantissa * pow(10, (double)signed_exponent);

    return ISEG_OK;
}

/// Gets the presently set current trip in resolution of range
/// \param serial_port file descriptor returned from ISEG_SHQ226L_connect
/// \param channel 1 or 2
/// \param reply holds error message or other info
/// \param trip in resolution of range
/// \param range ISEG_A = amps, ISEG_mA = milliamps, ISEG_uA = microamps
/// \return ISEG_COM_ERROR on error or ISEG_OK on all clear
int ISEG_SHQ226L_get_current_trip(const int serial_port, const int channel, char* reply, unsigned int* trip, unsigned int range, char* error)
{
    char msg[ISEG_MSG_SIZE];
    char iseg_reply[ISEG_MSG_SIZE];
    char err[ISEG_MSG_SIZE];

    long mantissa = 0, signed_exponent = 0;

    memset(error,'\0',sizeof(char)*ISEG_MSG_SIZE);
    memset(reply,'\0',sizeof(char)*ISEG_MSG_SIZE);

    switch(range)
    {
        case ISEG_A:
            sprintf(msg, "L%d", channel);
            break;
        case ISEG_mA:
            sprintf(msg, "LB%d", channel);
            break;
        case ISEG_uA:
            sprintf(msg, "LS%d", channel);
            break;
        default:
            sprintf(msg, "L%d", channel);
            break;
    }
    if(ISEG_SHQ226L_write(serial_port, msg, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    // ISEG replies with the command you sent, new line, then the response. So, we need to read twice
    if(ISEG_SHQ226L_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    memset(iseg_reply,'\0',sizeof(char)*ISEG_MSG_SIZE);
    if(ISEG_SHQ226L_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}

    strcpy(reply, iseg_reply);
    // parse current limit from reply string
    char* pEnd;
    mantissa = strtol(iseg_reply, &pEnd,10);
    signed_exponent = strtol(pEnd, &pEnd,10);
    //*trip = mantissa * pow(10, (double)signed_exponent);

    return ISEG_OK;
}

/// Sets the current trip in resolution of range
/// \param serial_port file descriptor returned from ISEG_SHQ226L_connect
/// \param channel 1 or 2
/// \param reply holds error message or other info
/// \param trip in resolution of range
/// \param range ISEG_A = amps, ISEG_mA = milliamps, ISEG_uA = microamps
/// \return ISEG_COM_ERROR on error or ISEG_OK on all clear
int ISEG_SHQ226L_set_current_trip(const int serial_port, const int channel, char* reply, unsigned int trip, unsigned int range, char* error)
{
    char msg[ISEG_MSG_SIZE];
    char iseg_reply[ISEG_MSG_SIZE];
    char empty_reply[ISEG_MSG_SIZE];
    char err[ISEG_MSG_SIZE];

    long mantissa = 0, signed_exponent = 0;

    memset(reply,'\0',sizeof(char)*ISEG_MSG_SIZE);
    memset(error,'\0',sizeof(char)*ISEG_MSG_SIZE);

    switch(range)
    {
        case ISEG_A:
            sprintf(msg, "L%d=%u", channel, trip);
            break;
        case ISEG_mA:
            sprintf(msg, "LB%d=%u", channel, trip);
            break;
        case ISEG_uA:
            sprintf(msg, "LS%d=%u", channel, trip);
            break;
        default:
            sprintf(msg, "L%d=%u", channel, trip);
            break;
    }
    if(ISEG_SHQ226L_write(serial_port, msg, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}
    // ISEG replies with the command you sent, new line, then the response. So, we need to read twice
    if(ISEG_SHQ226L_read(serial_port, iseg_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}

    if(ISEG_SHQ226L_read(serial_port, empty_reply, err)){sprintf(error, "%s",err); return ISEG_COM_ERROR;}

    strcpy(reply, iseg_reply);
    return ISEG_OK;
}
