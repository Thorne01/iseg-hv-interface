#ifndef ISEG_SHQ226L_ISEG_SHQ226L_H
#define ISEG_SHQ226L_ISEG_SHQ226L_H

#define ISEG_MSG_SIZE   512
#define ISEG_OK         0x0000 //means zero in decimal, equivalent to FUGHCB_SUCCESS
#define ISEG_COM_ERROR  0x0001 //means one in decimal, equivalent to FUGHCB_FAILURE
#define ISEG_HW_ERROR   0x0002

#define ISEG_A      0x0010
#define ISEG_mA     0x0011
#define ISEG_uA     0x0012

#define ISEG_STATUS_ON      0x0100
#define ISEG_STATUS_OFF     0x0101
#define ISEG_STATUS_MAN     0x0102
#define ISEG_STATUS_ERR     0x0103
#define ISEG_STATUS_INH     0x0104
#define ISEG_STATUS_QUA     0x0105
#define ISEG_STATUS_L2H     0x0106
#define ISEG_STATUS_H2L     0x0107
#define ISEG_STATUS_LAS     0x0108
#define ISEG_STATUS_TRP     0x0109
#define ISEG_STATUS_UNK     0x010A

#define ISEG_ERROR_SYNTAX   0x1000
#define ISEG_ERROR_WCN      0x1001
#define ISEG_ERROR_TOT      0x1002
#define ISEG_ERROR_UMAX     0x1003
#define ISEG_ERROR_UKN      0x1004

#define ISEG_CONNECT_FAIL (-1)

int ISEG_SHQ226L_connect(const char* port_id, char* error);
int ISEG_SHQ226L_disconnect(const int serial_port, char* error);

int ISEG_SHQ226L_write(const int serial_port, char* message, char* error);
int ISEG_SHQ226L_read(const int serial_port, char* reply, char* error);

int ISEG_SHQ226L_identify(const int serial_port, char* reply, char* error);

int ISEG_SHQ226L_get_status(const int serial_port, const int channel, char* reply, char* error);
int ISEG_SHQ226L_get_status_message(char* status_code, char* reply);

int ISEG_SHQ226L_get_error_message(char* error_code, char* error_message);

int ISEG_SHQ226L_set_ramp_rate(const int serial_port, const int channel, char* reply, const int ramp_rate, char* error);
int ISEG_SHQ226L_get_ramp_rate(const int serial_port, const int channel, char* reply, int* ramp_rate, char* error);
int ISEG_SHQ226L_start_ramp(const int serial_port, const int channel, char* reply, char* error);

int ISEG_SHQ226L_get_voltage(const int serial_port, const int channel, char* reply, double* voltage, char* error);
int ISEG_SHQ226L_set_voltage(const int serial_port, const int channel, char* reply, const double target_voltage, char* error);

int ISEG_SHQ226L_measure_voltage(const int serial_port, const int channel, char* reply, double* voltage, char* error);
int ISEG_SHQ226L_measure_current(const int serial_port, const int channel, char* reply, double* current, char* error);

int ISEG_SHQ226L_get_voltage_limit(const int serial_port, const int channel, char* reply, double* limit, char* error);
int ISEG_SHQ226L_get_current_limit(const int serial_port, const int channel, char* reply, double* limit, char* error);

int ISEG_SHQ226L_get_current_trip(const int serial_port, const int channel, char* reply, unsigned int* trip, unsigned int range, char* error);
int ISEG_SHQ226L_set_current_trip(const int serial_port, const int channel, char* reply, unsigned int trip, unsigned int range, char* error);


#endif //ISEG_SHQ226L_ISEG_SHQ226L_H
